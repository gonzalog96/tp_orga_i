; Trabajo Practico - Materia: Organizacion del Computador
;               < Primer semestre del 2017 >
; -------------------------------------------------
; Docentes: Jheisson Restrepo (jalopez@ungs.edu.ar)
;           Laura Almada (lalmada@ungs.edu.ar)
; -------------------------------------------------
; Alumnos: Uncos, Sergio (sergiofox_37@hotmail.com)
;          Arias, Emanuel (almafuerte948@gmail.com)
;          Godoy, Gonzalo (gonzalo_g96@hotmail.com)
; -------------------------------------------------

; VERSION FINAL (28/06/2017)
name "TP ORGA I (01/2017) - Uncos, Arias, Godoy"
org 100h

; saltar a main: desde aqui se invocaran todos los procedimientos necesarios
; para la ejecucion del programa.
JMP main

; variables.
FILAS equ 5
COLUMNAS equ 5

; grilla por defecto.
GRILLA db "....."
       db ".*..."
       db ".*.*."
       db ".**.."
       db "....."

CAMBIOS db FILAS*COLUMNAS DUP (?,?,?)
VIVO equ "*"
MUERTO equ "."
EOL db 10,13,'$'
  
; comienzo del programa.    
main:      
    ; recorriendo la grilla.
    recorrer:
        ; limpiar registros.
        XOR SI, SI
        XOR AX, AX 
        XOR BX, BX
        XOR DX, DX 
        XOR CX, CX
        
        ; posicionar cursor.
        MOV AH, 02h
        MOV DL, 0
        MOV DH, 0
        INT 10h
        
        ; mostrar la grilla.
        CALL MOSTRAR_GRILLA
        
        ; chequea los cambios que se deben hacer en la siguiente generacion.
        ; los guarda en el arreglo de cambios, para luego ser ejecutados por aplicar_cambios.
        CALL RELEVAR_CAMBIOS
    
        ; aplicar los cambios correspondientes a la grilla.
        CALL APLICAR_CAMBIOS
        
        ; cortar si se presiona una tecla.
        MOV AH, 0Bh
        INT 21h
        CMP AL, 0FFh
        JE SALIR
        
        ; recorrer la grilla nuevamente.
        JMP recorrer
        
        ; salir del programa.
        SALIR:
            RET

; *** PROCEDIMIENTOS GENERALES ***
; este procedimiento recibe una fila en BX y una columna en DI y devuelve un correspondiente indice en SI.
INDICE PROC NEAR
    ; guardar valores originales de AX, BX y CX.
    PUSH AX
    PUSH BX
    PUSH CX
    
    ; validaciones
    ; 1) se verifica que la fila no sea negativa y que no exceda a la cant. de filas maxima.
    ; 2) se verifica que la columna no sea negativa y que no excenda a la cant. de columnas maxima.
    CMP BX, 0
    JGE BIEN_1
    MOV BX, FILAS-1
    
    BIEN_1:
        CMP DI, 0
        JGE BIEN_2
        MOV DI, COLUMNAS-1
        
    BIEN_2:
        CMP BX, FILAS
        JL BIEN_3
        XOR BX, BX
        
    BIEN_3:
        CMP DI, COLUMNAS
        JL BIEN_FINAL
        XOR DI, DI
        
    BIEN_FINAL:        
        MOV AX, BX
        MOV CX, COLUMNAS
        MUL CL
        ADD AX, DI
        MOV SI, AX
    
    ; restauro valores originales de CX, BX y AX.
    finalizar_indice:
        POP CX
        POP BX
        POP AX
        RET
INDICE ENDP

; este procedimiento recibe una fila en BX y una columna en DI y devuelve en CL la cant. de vecinos vivos de una celula en particular.
CONTAR_VECINOS PROC NEAR
    ; guardo los valores originales de BX, DI y SI.
    PUSH BX
    PUSH DI
    PUSH SI
        
        ; limpio el registro CL.
        XOR CL, CL
        
        ; resto uno a la posicion de la columna.
        DEC DI
        
        ; llamo a indice para que me devuelva en SI la posicion de la celula.
        CALL INDICE
        
        ; verifico si este individuo (en la posicion SI) esta vivo.
        CMP GRILLA[SI], VIVO
        JNE NO_INC_1
        INC CL
    
    ; las siguientes etiquetas verifican si los individuos vecinos/lindantes a determinada celula
    ; se encuentran vivos. Por ejemplo:
    ; [*.*]
    ; [*?*]
    ; [**.]
    ; siendo "?" la celula huesped, "." los vecinos muertos (no se cuentan) y "*" los vecinos vivos.
    ; en total se chequean 8 posiciones.
    ; en este ejemplo, la celula "?" tiene 6 vecinos vivos.    
    NO_INC_1:
       DEC BX
       CALL INDICE
       CMP GRILLA[SI], VIVO
       JNE NO_INC_2
       INC CL
       
    NO_INC_2:
        INC DI
        CALL INDICE
        CMP GRILLA[SI], VIVO
        JNE NO_INC_3
        INC CL
        
    NO_INC_3:
        INC DI
        CALL INDICE
        CMP GRILLA[SI], VIVO
        JNE NO_INC_4
        INC CL
             
    NO_INC_4:         
        INC BX
        CALL INDICE
        CMP GRILLA[SI], VIVO
        JNE NO_INC_5
        INC CL
         
    NO_INC_5:     
        INC BX
        CALL INDICE
        CMP GRILLA[SI], VIVO
        JNE NO_INC_6
        INC CL
    
    NO_INC_6:    
        DEC DI
        CALL INDICE
        CMP GRILLA[SI], VIVO
        JNE NO_INC_7
        INC CL
        
    NO_INC_7:
        DEC DI
        CALL INDICE
        CMP GRILLA[SI], VIVO
        JNE NO_INC_8
        INC CL
        
    NO_INC_8:
        ; restauro valores originales de SI, DI y BX.
        POP SI
        POP DI
        POP BX
        
        ; finalizo el procedimiento.
        salir_contar_vecinos:  
            RET      
CONTAR_VECINOS ENDP

; este procedimiento recorre la grilla completa y la modifica en base a los cambios-
; que se deben realizar en la siguiente generacion.
; Estos cambios se guardan en el arreglo de CAMBIOS (OFFSET, CARACTER DE REEMPLAZO).
RELEVAR_CAMBIOS PROC NEAR
    ; respaldar valores originales de BP y SI. 
    PUSH BP
    PUSH SI
    
    ; apunto el BP a la primera posicion de CAMBIOS.
    MOV BP, offset CAMBIOS

    ; recorro cada celula de la grilla y chequeo la cant. vecinos vivos que tiene.
    RECORRO_GRILLA:
        ; si la columna (DI) llego a su maximo, reiniciarla e incrementar en 1 la fila (BX).
        CMP DI, COLUMNAS
        JE REINICIAR_COLUMNA_INC_FILA:
        
        ; si la fila llego a 5, entonces termine de recorrer la grilla: finalizo el procedimiento.
        CMP BX, FILAS
        JE FIN
        
        ; llamo a INDICE para que me devuelva en SI la posicion de la celula en particular.
        CALL INDICE
        
        ; llamo a CONTAR_VECINOS para que me devuelva en CL la cant. de vecinos vivos de la celula en particular anterior.
        CALL CONTAR_VECINOS
        
        ; verifico si estoy vivo o no.
        CMP GRILLA[SI], VIVO
        JE ESTOY_VIVO
        
        ; estoy muerto: chequeo si la cantidad de vecinos vivos que tengo.
        ; si es 3, guardo cambios; sino, verifico si tengo que avanzar o terminar.
        ESTOY_MUERTO:
            CMP CL, 3
            JE GUARDAR_CAMBIOS
            JNE AVANZAR_O_TERMINAR
            
        ; estoy vivo: chequeo cuantos vecinos vivos tengo.
        ; si tengo 2, no realizo cambios; si no tengo 2 y tengo 3, verifico.
        ESTOY_VIVO:
            CMP CL, 2
            JE AVANZAR_O_TERMINAR
            CMP CL, 3
            JE AVANZAR_O_TERMINAR
            
        ; guardo los correspondientes cambios.
        GUARDAR_CAMBIOS:
            ; verifico el estado de la celula. 
            CMP GRILLA[SI], MUERTO
            ; si estoy muerto, entonces revivo.
            JE VIVIR
            ; si estoy vivo, entonces muero.
            JNE MORIR
            
            ; almaceno los distintos cambios para la siguiente generacion.
            MORIR:
                ; guardo en BP el offset correspondiente a la celula.
                MOV [BP], SI
                
                ; incremento en 2 el bp para avanzar a la siguiente posicion (donde se guarda el caracter).
                ADD BP, 2

                ; asigno el caracter de reemplazo correspondiente e incremento el BP.
                MOV [BP], MUERTO
                INC BP
                    
                ; verifico si puedo avanzar a otra posicion o si finaliza el procedimiento.
                JMP AVANZAR_O_TERMINAR
              
            VIVIR:
                ; guardo en BP el offset correspondiente a la celula.
                MOV [BP], SI
                
                ; incremento en 2 el bp para avanzar a la siguiente posicion (donde se guarda el caracter).
                ADD BP, 2

                ; asigno el caracter de reemplazo correspondiente e incremento el BP.
                MOV [BP], VIVO
                INC BP
                
        ; verifico si llegue al final de la grilla.
        ; si llegue, finalizo el procedimiento; sino, avanzo de columna y comienzo de vuelta.    
        AVANZAR_O_TERMINAR:
            CMP SI, FILAS*COLUMNAS
            JE FIN
            
            ; avanzo de columna.
            INC DI
            JMP RECORRO_GRILLA
     
     ; incrementar la fila en 1 (bx) y reiniciar a cero la columna (DI).
     REINICIAR_COLUMNA_INC_FILA:
        XOR DI, DI
        INC BX
        JMP RECORRO_GRILLA
            
     ; fin del procedimiento.       
     FIN:
         POP SI
         POP BP   
         RET
RELEVAR_CAMBIOS ENDP

; este procedimiento recorre la grilla y, en base a la secuencia de cambios, va modificando
; la grilla con los cambios que se mostraran en la siguiente iteracion.
APLICAR_CAMBIOS PROC NEAR
    PUSH BX
    PUSH BP
    
    ; limpio el registro DX.
    XOR DX, DX
    
    ; apunto a la primer posicion de los cambios.
    MOV BP, offset CAMBIOS
    
    ; comienzo de la secuencia. 
    recorrer_secuencia:
            ; cargo en BX la celula a modificar.
            MOV BX, [BP]
            
            ; verificar que el contenido de BP no sea cero (terminaron los cambios).
            ; si es cero, verificar que el caracter NO SEA NULO.
            CMP BX, 0
            JNE CONTINUAR
            
            ; el offset es cero: verifico que el caracter no sea null.
            VERIFICAR_CARACTER_NO_NULO:
                CMP [BP+2], 0x00
                JE FINAL_PROC
            
            CONTINUAR:
                ; cargo en DL el caracter de reemplazo y aplico los cambios en la grilla.
                MOV DL, [BP+2]
                MOV GRILLA[BX], DL 
        
        ; me posiciono en el proximo indice de celula guardado en el arreglo de cambios.    
        AVANZAR_POSICION:
            ; incremento en 3 el BP (para llegar hasta el indice de la celula). 
            ADD BP, 3
            ; recorro nuevamente la secuencia para verificar si hay mas cambios que realizar.
            JMP recorrer_secuencia
        
    ; finalizar procedimiento.
    FINAL_PROC:
        ; limpio por completo el arreglo de cambios.
        XOR SI, SI
        limpiar_cambios:
            MOV CAMBIOS[SI], 0
            INC SI
            CMP SI, FILAS*COLUMNAS
            JNE limpiar_cambios
                
        ; restauro el valor de SI.
        POP BP
        POP BX
        RET
APLICAR_CAMBIOS ENDP
 
; este procedimiento recorre la grilla por completo, mostrando por pantalla su contenido.
MOSTRAR_GRILLA PROC NEAR 
    ; comienzo del procedimiento.
    recorrido_normal:
        ; verifico si llegue al final de la grilla.
        CMP SI, FILAS*COLUMNAS
        JE terminar_recorrido_grilla  
        
        ; muestro el contenido de la grilla en la posicion SI por pantalla.    
        MOV DL, GRILLA[SI]
        MOV AH, 02h
        INT 21h
        
        ; avanzo de posicion e incremento el BP (se utiliza para ver si llegue al final de la columna).
        INC SI
        INC BP
        
        ; verifico si llegue al final de la columna: si llegue, avanzo una fila; sino, vuelvo al recorrido normal.
        CMP BP, COLUMNAS
        JNE recorrido_normal
        
        ; avanzo una fila y coloco en cero la columna.
        reiniciar_columna_avanzar_fila:
            ; coloco en cero el BP.
            XOR BP, BP
            
            ; inserto un salto de linea mas retorno de carro.
            MOV DX, offset EOL
            MOV AH, 09h
            INT 21h
            
            ; salto hacia el recorrido normal.
            JMP recorrido_normal
    
    ; termino el procedimiento.    
    terminar_recorrido_grilla:
        RET
MOSTRAR_GRILLA ENDP